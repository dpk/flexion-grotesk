import sys

from defcon import Font
from ufo2ft import compileOTF
from ufo2ft.filters.removeOverlaps import RemoveOverlapsFilter


if __name__ == '__main__':
    ufo = Font(sys.argv[1])
    otf = compileOTF(
        ufo,
        removeOverlaps=True,
        overlapsBackend=RemoveOverlapsFilter.Backend.SKIA_PATHOPS
    )
    otf.save(sys.argv[2])
else:
    raise ImportError('ufo2otf is a script, not a module')
