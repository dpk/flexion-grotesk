# Flexion Grotesk

Flexion Grotesk (pronounced as in German, with a real ‘ks’ sound for the x in Flexion) is a revival of Akzidenz Grotesk. It is based on [CC Accidenz Grotesk](https://github.com/jkocontreras/Accidenz-CC) by Joaquín Contreras and Miguel Hernández Montoya. Like CC Accidenz Grotesk, it is CC BY-SA.

![](specimen.png)

(Specimen as of 12 April 2021.)

## Differences and Additions vs CC Accidenz Grotesk

- Almost complete set of quotation marks (guillemets still missing).
- Eszett character.
- More accented characters (complete support for German, with more languages on the way).
- British pound, Japanese yen, and US cent signs.
- Hash sign.

## To come

- More accented characters and punctuation symbols.
- Lighter and maybe heavier masters (variable font, maybe).
