.PHONY: website clean

website: public/index.html public/style.css public/reset.css public/fonts/Flexion.woff2 public/fonts/Flexion.woff public/dl/Flexion-OTF.tar.gz public/dl/Flexion-WOFF.tar.gz

clean:
	rm -rf build public


build:
	mkdir -p build

build/%.otf: %.ufo | build
	python3 script/ufo2otf.py $< $@

build/%.ttf: build/%.otf | build
	otf2ttf -o /dev/stdout $< | ttfautohint > $@
build/%.woff: build/%.ttf | build
	ttf2woff $< $@
build/%.woff2: build/%.ttf | build
	woff2_compress $<

build/Flexion-OTF.tar.gz: build/Flexion.otf
	echo $< | pax -w -s '/^build\//Flexion-OTF\//' | gzip -9 > $@
build/Flexion-WOFF.tar.gz: build/Flexion.woff build/Flexion.woff2
	(echo build/Flexion.woff; echo build/Flexion.woff2) | pax -w -s '/^build\//Flexion-WOFF\//' | gzip -9 > $@

public:
	mkdir -p public

public/index.html: index.html | public
	sed "s!%%WHEN%%!<time datetime=$$(date '+%Y-%m-%d')>$$(date '+%e %B %Y')</time>!" $< > $@

public/%.css: %.css | public
	cp $< $@

public/fonts: | public
	mkdir -p public/fonts

public/fonts/%.woff: build/%.woff | public/fonts
	cp $< $@
public/fonts/%.woff2: build/%.woff2 | public/fonts
	cp $< $@

public/dl: | public
	mkdir -p public/dl

public/dl/%.tar.gz: build/%.tar.gz | public/dl
	cp $< $@
